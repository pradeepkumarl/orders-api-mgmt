package com.classpath.itemsapi.service;

import com.classpath.itemsapi.model.Order;
import com.classpath.itemsapi.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OrderService {

    private OrderRepository orderRepository;

    public List<Order> fetchOrders(){
        return this.orderRepository.findAll();
    }

    public Order saveOrder(Order order){
        return  this.orderRepository.save(order);
    }

    public Order fetchOrderById(long orderId){
        return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("Invalid order Id"));
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}