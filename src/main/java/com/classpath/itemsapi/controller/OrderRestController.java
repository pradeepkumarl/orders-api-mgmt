package com.classpath.itemsapi.controller;

import com.classpath.itemsapi.model.Order;
import com.classpath.itemsapi.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/orders")
@AllArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @GetMapping
    public List<Order> fetchOrders(){
        return this.orderService.fetchOrders();
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long orderId){
        return this.orderService.fetchOrderById(orderId);
    }

    @DeleteMapping("/{id}")
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }

    @PostMapping
    public Order saveOrder(@RequestBody Order order){
        return this.orderService.saveOrder(order);
    }

}