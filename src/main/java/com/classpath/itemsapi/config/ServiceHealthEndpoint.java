package com.classpath.itemsapi.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

@Component
public class ServiceHealthEndpoint implements HealthIndicator {

    @Override
    public Health health() {
        //connect to db and execute a commad
        //call remote api
        return Health.status(Status.UP).withDetail("Kafka-Service", "Kafka broker is up").build();
    }
}
@Component
class DBServiceHealthEndpoint implements HealthIndicator {

    @Override
    public Health health() {
        //connect to db and execute a commad
        //call remote api
        return Health.status(Status.UP).withDetail("DB-Service", "DB is up").build();
    }
}