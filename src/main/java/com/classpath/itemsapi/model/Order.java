package com.classpath.itemsapi.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

import static javax.persistence.GenerationType.AUTO;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of="orderId")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    private String customerName;

    private double orderPrice;

    private LocalDate orderDate;
}